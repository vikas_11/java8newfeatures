package com.java8features.supplier;

import java.util.function.IntSupplier;

public class IntSupplierExample {

	public static void main(String[] args) {
		IntSupplier intSupplier = () -> {
			return 1;
		};
		int i= intSupplier.getAsInt();
		System.out.println(i);
	}
}
