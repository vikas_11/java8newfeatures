package com.java8features.supplier;

import java.util.function.BooleanSupplier;

public class BooleanSupplierExample {

	public static boolean methodBody() {
		if ("" instanceof String) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		BooleanSupplier booleanSupplier = BooleanSupplierExample::methodBody;
		boolean bool = booleanSupplier.getAsBoolean();
		System.out.println(bool);
	}
}
