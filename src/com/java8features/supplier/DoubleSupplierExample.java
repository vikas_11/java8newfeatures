package com.java8features.supplier;

import java.util.function.DoubleSupplier;

public class DoubleSupplierExample {

	public static double methodBody() {
		return new Double(44);
	}

	public static void main(String[] args) {
		DoubleSupplier doubleSupplier = DoubleSupplierExample::methodBody;
		double d = doubleSupplier.getAsDouble();
		System.out.println(d);
	}
}
