package com.java8features.lambdaexp;

public class RunnableWithLambda {

	public static void main(String[] args) {
		Runnable r1 = ()->{
			for (int i = 0; i < 10; i++) {
				System.out.println("Child thread");
			}
		};
		
		Thread child = new Thread(r1);
		child.start();
		for (int i = 0; i < 10; i++) {
			System.out.println("Main thread");
		}
		
	}
}
