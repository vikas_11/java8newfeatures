package com.java8features.lambdaexp;

public class TestLambda {

	public static void main(String[] args) {
		// here compiler is used type inference to bind lambda to interface methods
		Interf i1 = () -> System.out.println("Lambda implemented");
		i1.m1();
	}
}
