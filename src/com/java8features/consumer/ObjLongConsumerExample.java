package com.java8features.consumer;

import java.util.function.ObjLongConsumer;

import com.java8features.model.Employee;

public class ObjLongConsumerExample {

	public static void main(String[] args) {
		ObjLongConsumer<Employee> objLongConsumer = (e1, l) -> System.out.println(e1.getSalary() + l);
		objLongConsumer.accept(new Employee(1002, "ABC", 3645.654), 5454654L);
	}
}
