package com.java8features.consumer;

import java.util.function.ObjDoubleConsumer;

import com.java8features.model.Employee;

public class ObjDoubleConsumerExample {

	public static void main(String[] args) {
		ObjDoubleConsumer<Employee> objDoubleConsumer = (e1, d)-> System.out.println(e1.getSalary() + d);
		objDoubleConsumer.accept(new Employee(21, "Vikas", 6545.454), 2454.4545);
	}
}
