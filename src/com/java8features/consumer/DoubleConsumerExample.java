package com.java8features.consumer;

import java.util.function.DoubleConsumer;

public class DoubleConsumerExample {

	public static void main(String[] args) {
		DoubleConsumer doubleConsumer = d -> System.out.println(d);
		doubleConsumer.accept(5685.3568);
	}
}
