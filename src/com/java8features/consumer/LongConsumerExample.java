package com.java8features.consumer;

import java.util.function.LongConsumer;

public class LongConsumerExample {
	public static void main(String[] args) {
		LongConsumer longConsumer = l -> System.out.println(l);
		longConsumer.accept(654676845L);
	}
}
