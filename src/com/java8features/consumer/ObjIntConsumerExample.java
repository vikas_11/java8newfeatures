package com.java8features.consumer;

import java.util.function.ObjIntConsumer;

import com.java8features.model.Employee;

public class ObjIntConsumerExample {

	public static void main(String[] args) {
		ObjIntConsumer<Employee> objIntConsumer = (e1, i) -> System.out.println(e1.getSalary() + i);
		objIntConsumer.accept(new Employee(101, "Vikas", 6545.454), 1);
	}
}
