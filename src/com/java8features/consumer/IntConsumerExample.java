package com.java8features.consumer;

import java.util.function.IntConsumer;

public class IntConsumerExample {

	public static void main(String[] args) {
		IntConsumer intConsumer = i -> System.out.println(i * i);
		intConsumer.accept(5);
	}
}
