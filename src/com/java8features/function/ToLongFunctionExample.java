package com.java8features.function;

import java.util.function.ToLongFunction;

public class ToLongFunctionExample {

	public static void main(String[] args) {
		ToLongFunction<Integer> toLongFunc = inte -> inte * inte;
		System.out.println(toLongFunc.applyAsLong(435643554));
	}
}
