package com.java8features.function;

import java.util.function.ToDoubleBiFunction;

public class ToDoubleBiFunctionExample {

	public static void main(String[] args) {
		ToDoubleBiFunction<Long, Integer> toDoubleBiFunc = (l, i) -> i * l;
		System.out.println(toDoubleBiFunc.applyAsDouble(45643L, 3523));
	}
}
