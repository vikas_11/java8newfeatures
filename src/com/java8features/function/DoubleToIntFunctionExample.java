package com.java8features.function;

import java.util.function.DoubleToIntFunction;

public class DoubleToIntFunctionExample {

	public static void main(String[] args) {
		DoubleToIntFunction doubleToIntFunc = d -> (int) Math.round(d);
		System.out.println(doubleToIntFunc.applyAsInt(564.3652));
	}
}
