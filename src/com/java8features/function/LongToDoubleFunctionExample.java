package com.java8features.function;

import java.util.function.LongToDoubleFunction;

public class LongToDoubleFunctionExample {

	public static void main(String[] args) {
		LongToDoubleFunction longToDoubleFunc = l -> l;
		System.out.println(longToDoubleFunc.applyAsDouble(35745454654L));
	}
}
