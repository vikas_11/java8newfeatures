package com.java8features.function;

import java.util.function.IntToLongFunction;

public class IntToLongFunctionExample {

	public static void main(String[] args) {
		IntToLongFunction intToLongFunc = i -> i * i;
		System.out.println(intToLongFunc.applyAsLong(1546456454));
	}
}
