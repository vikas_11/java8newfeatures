package com.java8features.function;

import java.util.function.IntToDoubleFunction;

public class IntToDoubleFunctionExample {

	public static void main(String[] args) {
		IntToDoubleFunction intToDoubleFunc = i -> i * i;
		System.out.println(intToDoubleFunc.applyAsDouble(13));
	}
}
