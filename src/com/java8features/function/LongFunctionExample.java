package com.java8features.function;

import java.util.function.LongFunction;

public class LongFunctionExample {

	public static void main(String[] args) {
		LongFunction<String> lf = l -> "" + l;
		System.out.println(lf.apply(435753454654L));
	}
}
