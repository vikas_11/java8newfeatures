package com.java8features.function;

import java.util.function.ToIntFunction;

public class ToIntFunctionExample {

	public static void main(String[] args) {
		ToIntFunction<String> toIntFunc = s1 -> s1.length();
		System.out.println(toIntFunc.applyAsInt("Vikas"));
	}
}
