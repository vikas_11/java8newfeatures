package com.java8features.function;

import java.util.function.DoubleToLongFunction;

public class DoubleToLongFunctionExample {

	public static void main(String[] args) {
		DoubleToLongFunction doubleToLongFunc = d -> Math.round(d);
		System.out.println(doubleToLongFunc.applyAsLong(5453424554L));
	}
}
