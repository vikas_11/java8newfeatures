package com.java8features.function;

import java.util.function.ToIntBiFunction;

public class ToIntBiFunctionExample {

	public static void main(String[] args) {
		ToIntBiFunction<Double, Long> toIntBiFunc = (d, l) -> d.intValue() * l.intValue();
		System.out.println(toIntBiFunc.applyAsInt(58.365, 354554L));
	}
}
