package com.java8features.function;

import java.util.function.DoubleFunction;

public class DoubleFunctionExample {

	public static void main(String[] args) {
		DoubleFunction<String> df = d -> "" + d;
		System.out.println(df.apply(565.3565));
	}
}
