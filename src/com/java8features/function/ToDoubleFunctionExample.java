package com.java8features.function;

import java.util.function.ToDoubleFunction;

public class ToDoubleFunctionExample {
	public static void main(String[] args) {
		ToDoubleFunction<Integer> toDoubleFunc = i -> i * i;
		System.out.println(toDoubleFunc.applyAsDouble(6454));
	}
}
