package com.java8features.function;

import java.util.function.IntFunction;

public class IntFunctionExample {

	public static void main(String[] args) {
		IntFunction<String> iFunc = i -> "" + i;
		System.out.println(iFunc.apply(5));
	}
}
