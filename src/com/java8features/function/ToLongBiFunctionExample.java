package com.java8features.function;

import java.util.function.ToLongBiFunction;

public class ToLongBiFunctionExample {

	public static void main(String[] args) {
		ToLongBiFunction<Integer, Float> toLongBiFunc = (inte, shorte) -> (long) (inte * shorte);
		System.out.println(toLongBiFunc.applyAsLong(54, 456F));
	}
}
