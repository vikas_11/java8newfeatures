package com.java8features.function;

import java.util.function.LongToIntFunction;

public class LongToIntFunctionExample {

	public static void main(String[] args) {
		LongToIntFunction longToIntFunc = l -> (int) l;
		System.out.println(longToIntFunc.applyAsInt(46546546545454654L));
	}
}
