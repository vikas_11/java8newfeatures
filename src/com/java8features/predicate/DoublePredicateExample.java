package com.java8features.predicate;

import java.util.function.DoublePredicate;

public class DoublePredicateExample {
	public static void main(String[] args) {
		DoublePredicate dp = i -> i < 10.53685;
		System.out.println(dp.test(4.26654));
	}
}
