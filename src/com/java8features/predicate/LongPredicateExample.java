package com.java8features.predicate;

import java.util.function.LongPredicate;

public class LongPredicateExample {

	public static void main(String[] args) {
		LongPredicate lp = i -> i < 46754567645767L;
		System.out.println(lp.test(24575455454L));
	}
}
