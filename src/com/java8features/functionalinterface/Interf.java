package com.java8features.functionalinterface;

@FunctionalInterface
public interface Interf {

/*	   a) if interface contains exactly one abstract method(S-A-M).
		ex : Runnable, Callable, ActionListener, Comparable
	   b) any number of default method.
	   c) any number of static method.*/
	
	// single abstract method minimally
	public abstract void abstractM1();
	
	public static void staticM1()
	{
		System.out.println("Interf.staticM1()");
	}
	
	public static void staticM2()
	{
		System.out.println("Interf.staticM1()");
	}
	
	default void defaultM1()
	{
		System.out.println("Interf.defaultM1()");
	}
	
	default void defaultM2()
	{
		System.out.println("Interf.defaultM1()");
	}
}
